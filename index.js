const {sequelize, Utilizator, Proiect, Livrabil} = require("./models");
const express = require("express");
const bodyParser = require("body-parser");
const app = express();
app.use(bodyParser.json());

app.post('/users', async (request, response) => {
    try{
        const utilizator = request.body;
    if(utilizator.nume_utilizator && utilizator.parola && utilizator.rol && utilizator.email)
    {
       await Utilizator.create(utilizator);
            response.status(201).send({
                message: 'Utilizator creat cu succes'
            });
    }
    else{
        response.status(400).send({
            message: 'Utilizator invalid'
        });
    }}
    catch (err) {
        console.log(err);
        response.status(500).json({message: `Eroare: ${err}`})
    }
});

app.post('/projects', async (request, response) => {
    try{
        const project = request.body;
    if(project.nume_proiect)
    {
       await Proiect.create(project);
            response.status(201).send({
                message: 'Proiect creat cu succes'
            });
    }
    else{
        response.status(400).send({
            message: 'Proiect invalid'
        });
    }}
    catch (err) {
        console.log(err);
        response.status(500).json({message: `Eroare: ${err}`})
    }
});

app.post('/deliverables', async (request, response) => {
    try{
        const deliverables = request.body;
    if(deliverables.id_proiect && deliverables.nr_livrabil && deliverables.share && deliverables.data_livrare)
    {
       await Livrabil.create(deliverables);
            response.status(201).send({
                message: 'Livrabil creat cu succes'
            });
    }
    else{
        response.status(400).send({
            message: 'Livrabil invalid'
        });
    }}
    catch (err) {
        console.log(err);
        response.status(500).json({message: `Eroare: ${err}`})
    }
});

app.delete('/users/:email', async (request, response) => {
     try {
        let utilizator = await Utilizator.findByPk(request.params.email)
        if (utilizator){
            await utilizator.destroy()
            response.status(202).json({message : 'Utilizator sters.'})
        }
        else{
            response.status(404).json({message : 'Utilizatorul nu a fost gasit.'})
        }
    } catch (err) {
        console.log(err);
        response.status(500).json({message: `Eroare: ${err}`})
    }
})

app.delete('/deliverables/:id', async (request, response) => {
     try {
        let livrabil = await Livrabil.findByPk(request.params.id)
        if (livrabil){
            await livrabil.destroy()
            response.status(202).json({message : 'Livrabil sters.'})
        }
        else{
            response.status(404).json({message : 'Livrabilul nu a fost gasit.'})
        }
    } catch (err) {
        console.log(err);
        response.status(500).json({message: `Eroare: ${err}`})
    }
})

app.delete('/projects/:id', async (request, response) => {
     try {
        let project = await Proiect.findByPk(request.params.id)
        if (project){
            await project.destroy()
            response.status(202).json({message : 'Proiect sters.'})
        }
        else{
            response.status(404).json({message : 'Proiectul nu a fost gasit.'})
        }
    } catch (err) {
        console.log(err);
        response.status(500).json({message: `Eroare: ${err}`})
    }
})

app.put('/users/:email', async (request, response) => {
    try {
        let utilizator = await Utilizator.findByPk(request.params.email)
        if (utilizator){
            await utilizator.update(request.body)
            response.status(202).json({message : 'Modificare efectuata.'})
        }
        else{
            response.status(404).json({message : 'Utilizator negasit.'})
        }
    } catch(err){
        console.warn(err);
        response.status(500).json({
            message: 'Eroare de server'
        })
    }
})

app.put('/deliverables/:id', async (request, response) => {
    try {
        let livrabil = await Livrabil.findByPk(request.params.id)
        if (livrabil){
            await livrabil.update(request.body)
            response.status(202).json({message : 'Modificare efectuata.'})
        }
        else{
            response.status(404).json({message : 'Livrabil negasit.'})
        }
    } catch(err){
        console.warn(err);
        response.status(500).json({
            message: 'Eroare de server'
        })
    }
})

app.put('/projects/:id', async (request, response) => {
    try {
        let project = await Proiect.findByPk(request.params.id)
        if (project){
            await project.update(request.body)
            response.status(202).json({message : 'Modificare efectuata.'})
        }
        else{
            response.status(404).json({message : 'Proiect negasit.'})
        }
    } catch(err){
        console.warn(err);
        response.status(500).json({
            message: 'Eroare de server'
        })
    }
})

app.get('/deliverables/:id', async (request, response) => {
    try{
        let livrabil = await Livrabil.findByPk(request.params.id);
    if (livrabil){
        response.status(200).json(livrabil)
    }
    else{
        response.status(404).json({
            message: 'Livrabilul nu a fost gasit'
        });
    }
    }
    catch(err){
        console.warn(err);
        response.status(500).json({
            message: 'Eroare de server'
        })
    }
});

app.get('/projects/:id', async (request, response) => {
    try{
        let project = await Proiect.findByPk(request.params.id);
    if (project){
        response.status(200).json(project)
    }
    else{
        response.status(404).json({
            message: 'Proiectul nu a fost gasit'
        });
    }
    }
    catch(err){
        console.warn(err);
        response.status(500).json({
            message: 'Eroare de server'
        })
    }
});

app.get('/users/:email', async (request, response) => {
    try{
        let utilizator = await Utilizator.findByPk(request.params.email);
    if (utilizator){
        response.status(200).json(utilizator)
    }
    else{
        response.status(404).json({
            message: 'Utilizatorul nu a fost gasit'
        });
    }
    }
    catch(err){
        console.warn(err);
        response.status(500).json({
            message: 'Eroare de server'
        })
    }
});

app.listen(8080, () => {
    console.log('Serverul pornit pe port 8080.');
});
