const Sequelize = require("sequelize");
const sequelize = new Sequelize('ProiectPetrica','root', 'telemea', {
    dialect: 'mysql'
});
sequelize.authenticate().then(() => {
    console.log('Conectare reusita la baza de date.');
}).catch(err => {
    console.log(`Conectare nereusita la baza de date. Eroare: ${err}`);
});

class Utilizator extends Sequelize.Model {};
Utilizator.init({
    nume_utilizator: Sequelize.STRING,
    email: {type: Sequelize.STRING, primaryKey:true},
    parola: Sequelize.STRING,
    rol: Sequelize.STRING,
    id_proiect: Sequelize.INTEGER,
    id_proiect_notat: Sequelize.INTEGER,
    nota_acordata: Sequelize.FLOAT,
    data_acordare_nota: Sequelize.STRING
}, {sequelize, modelName: 'utilizator'});

class Proiect extends Sequelize.Model {};
Proiect.init({
    id_proiect: {type: Sequelize.INTEGER, primaryKey:true, autoIncrement:true},
    nume_proiect: Sequelize.STRING,
    video: Sequelize.STRING,
    link: Sequelize.STRING,
    nota_finala: Sequelize.FLOAT
}, {sequelize, modelName: 'proiect'});

class Livrabil extends Sequelize.Model {};
Livrabil.init({
    id_livrabil: {type:Sequelize.INTEGER, primaryKey:true, autoIncrement:true},
    id_proiect: Sequelize.INTEGER,
    nr_livrabil: Sequelize.INTEGER,
    share: Sequelize.STRING,
    data_livrare: Sequelize.STRING
}, {sequelize, modelName: 'livrabil'});

Proiect.hasMany(Utilizator, { foreignKey: 'id_proiect' });
Utilizator.belongsTo(Proiect, { foreignKey: 'id_proiect' });
Proiect.hasMany(Livrabil, { foreignKey: 'id_proiect' });
Livrabil.belongsTo(Proiect, { foreignKey: 'id_proiect' });

Utilizator.sync();
Livrabil.sync();
Proiect.sync();

module.exports = {
  sequelize,
  Utilizator,
  Proiect,
  Livrabil
};