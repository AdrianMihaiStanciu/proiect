import React from 'react';
import axios from 'axios';

class AddUser extends React.Component{
  constructor(props) {
      super(props);
      this.state = {
            username: '',
            password: '',
            users: []
        };
    }

    handleChangeUsername= (event) => {
        this.setState({
            username: event.target.value
        });
    }
    
    handleChangePassword = (event) => {
        this.setState({
            password: event.target.value
        });
    }
    
    loginToDB = () => {
      axios.get('http://3.19.238.112:8080/users/' + this.state.username).then(users => {
      this.setState({
        users: users.data
      })
    })
        console.log(this.state.users);
    }

    render() {
        return (
            <div>
            <h1>Bine ati venit!</h1>
            <input
                type="text"
                placeholder="Nume utilizator"
                value={this.state.username}
                onChange={this.handleChangeUsername}
            />
            <input
                type="text"
                placeholder="Parola"
                value={this.state.password}
                onChange={this.handleChangePassword}
                />
                <button
                onClick={this.loginToDB}>
                Login</button>
            </div>
            );
    }
}

export default AddUser;