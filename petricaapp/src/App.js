import React, { Component } from 'react';

import AddUser from './AddUser';
import UserStore from './UserStore';
import axios from 'axios';
import './App.css';

class App extends Component {
  constructor(props){
    super(props);
    this.state = {
      users: []
    };
  }

  componentDidMount = () => {
    axios.get('http://3.19.238.112:8080/users').then(users => {
      this.setState({
        users: users.data
      })
    })
  }
  
  render() {
    return (
      <div className="App">
          <AddUser />
          <UserStore users={this.state.users}/>
      </div>
    );
  }
}

export default App;
